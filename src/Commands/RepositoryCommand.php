<?php

namespace Captbrogers\Generators\Commands;

use Captbrogers\Generators\Traits\AppNamespaceDetectorTrait;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RepositoryCommand extends Command
{
    use AppNamespaceDetectorTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gen:repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new (opinionated) repository file';

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * @var Composer
     */
    private $composer;

    /**
     * @var $repositoryName
     */
    private $repositoryName;

    /**
     * @var $repositoryContractName
     */
    private $repositoryContractName;

    /**
     * @var $repositoryModelName
     */
    private $repositoryModelName;

    /**
     * @var $repositoryModelName
     */
    private $repositoryModelLowName;

    /**
     * @var $repositoryClassName
     */
    private $repositoryClassName;

    /**
     * @var $repositoryLowName
     */
    private $repositoryLowName;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     * @param Composer $composer
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repositoryName = $this->getRepositoryName($this->argument('name'));

        if ($this->files->exists($repositoryFile = $this->getRepositoryPath($this->repositoryName))) {
            return $this->error($this->repositoryName . 'Repository.php repository already exists!');
        } else {
            $this->makeDirectory($repositoryFile);
            $this->files->put($repositoryFile, $this->compileRepositoryStub());
            $this->info('Repository created successfully.');
        }

        $this->composer->dumpAutoloads();
    }

    /**
     * Get the class name for the repository generator.
     *
     * @return string
     */
    protected function getRepositoryName($inputString)
    {
        $className = ucwords(str_singular(camel_case($inputString)));
        $haystack = 'Repository';
        $repositoryName = $className;
        $temp = strlen($haystack) - strlen($repositoryName);

        if ($temp >= 1 and !strpos($haystack, $repositoryName, $temp)) {
            $repositoryName = $className . 'Repository';
        } else {
            $className = substr($className, 0, strlen($className - 10));
        }

        $this->repositoryClassName = $repositoryName;
        $this->repositoryContractName = $repositoryName.'Contract';
        $this->repositoryModelName = $className;
        $this->repositoryModelLowName = strtolower($className);
        return $repositoryName;
    }

    /**
     * Get the path to where we should store the repository.
     *
     * @param  string $name
     * @return string
     */
    protected function getRepositoryPath()
    {
        return $this->laravel['path'] . '/Repositories/Eloquent/' . $this->repositoryName . '.php';
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0755, true, true);
        }
    }

    /**
     * Compile the repository stub.
     *
     * @return string
     */
    protected function compileRepositoryStub()
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/repository.stub');

        $this->replaceRepositoryNamespace($stub)
            ->replaceClassName($stub)
            ->replaceModelNamespace($stub)
            ->replaceModelName($stub)
            ->replaceModelLowName($stub)
            ->replaceLowName($stub);

        return $stub;
    }

    /**
     * Replace the namespace in the repository stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceRepositoryNamespace(&$stub)
    {
        $stub = str_replace('{{namespace}}', $this->getAppNamespace() . 'Repositories\Eloquent', $stub);
        return $this;
    }

    /**
     * Replace the class name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceClassName(&$stub)
    {
        $stub = str_replace('{{class}}', $this->repositoryClassName, $stub);
        return $this;
    }

    /**
     * Replace the class name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceModelNamespace(&$stub)
    {
        $stub = str_replace('{{modelNamespace}}', $this->getAppNamespace() . 'Models', $stub);
        return $this;
    }

    /**
     * Replace the model class name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceModelName(&$stub)
    {
        $stub = str_replace('{{model}}', $this->repositoryModelName, $stub);
        return $this;
    }

    /**
     * Replace the model class low name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceModelLowName(&$stub)
    {
        $stub = str_replace('{{modellow}}', $this->repositoryModelLowName, $stub);
        return $this;
    }

    /**
     * Replace the class low name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceLowName(&$stub)
    {
        $stub = str_replace('{{classlow}}', strtolower($this->repositoryLowName), $stub);
        return $this;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            // [$name, $mode, $description, $defaultValue]
            ['name', InputArgument::REQUIRED, 'The name of the repository'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            // [$name, $shortcut, $mode, $description, $defaultValue]
        ];
    }
}
