<?php

namespace Captbrogers\Generators\Commands;

use Captbrogers\Generators\Traits\AppNamespaceDetectorTrait;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ModelCommand extends Command
{
    use AppNamespaceDetectorTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gen:model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new (opinionated) model with migration';

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * @var Composer
     */
    private $composer;

    /**
     * @var $modelName
     */
    private $modelName;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     * @param Composer $composer
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->modelName = $this->getModelName($this->argument('name'));

        if ($this->files->exists($modelFile = $this->getModelPath($this->modelName))) {
            return $this->error($this->modelName . '.php model already exists!');
        } else {
            $this->makeDirectory($modelFile);
            $this->files->put($modelFile, $this->compileModelStub());
            $this->info('Model created successfully.');
        }

        $this->generateMigrationFile();
        $this->generateSeedFile();

        if ($this->option('with-repository')) {
            $this->call('gen:repository', [
                'name' => $this->modelName
            ]);
        }

        $this->composer->dumpAutoloads();
    }

    /**
     * Generate the migration file associated with the model.
     *
     * @return void
     */
    public function generateMigrationFile()
    {
        $prefix = date('Y_m_d') . '_' . substr(time(), -6, 6) . '_create_';
        $lowercaseNamePlrl = str_plural(strtolower($this->argument('name')));
        $suffix = '_table.php';

        $migrationFileName = $prefix . $lowercaseNamePlrl . $suffix;

        $migrationFile = $this->getMigrationPath($migrationFileName);
        $this->files->put($migrationFile, $this->compileMigrationStub());
        $this->info('Migration created successfully.');
    }

    /**
     * Generate the seeder file associated with the model.
     *
     * @return void
     */
    public function generateSeedFile()
    {
        $prefixName = $this->transformUcPlural();
        $suffix = 'TableSeeder.php';
        $seedFileName = $prefixName . $suffix;

        $seedFile = $this->getSeedPath($seedFileName);
        $this->files->put($seedFile, $this->compileSeedStub());
        $this->info('Migration created successfully.');
    }

    /**
     * Get the class name for the Eloquent model generator.
     *
     * @return string
     */
    protected function getModelName($inputString)
    {
        return ucwords(str_singular(camel_case($inputString)));
    }

    /**
     * Get the path to where we should store the model.
     *
     * @param  string $name
     * @return string
     */
    protected function getModelPath()
    {
        return $this->laravel['path'] . '/Models/' . $this->modelName . '.php';
    }

    /**
     * Get the path to where we should store the model.
     *
     * @param  string $name
     * @return string
     */
    protected function getMigrationPath($migrationFileName)
    {
        return $this->laravel['path'] . '/../database/migrations/' . $migrationFileName;
    }

    /**
     * Get the path to where we should store the model.
     *
     * @param  string $name
     * @return string
     */
    protected function getSeedPath($seedFileName)
    {
        return $this->laravel['path'] . '/../database/seeds/' . $seedFileName;
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0755, true, true);
        }
    }

    /**
     * Take the name passed in, ensure it is "StudlyCase",
     * pluralized, and then returned.
     *
     * @return string
     */
    protected function transformUcPlural()
    {
        return ucwords(str_plural(camel_case($this->argument('name'))));
    }

    /**
     * Compile the model stub.
     *
     * @return string
     */
    protected function compileModelStub()
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/model.stub');

        $this->replaceModelNamespace($stub)
            ->replaceTableName($stub)
            ->replaceClassName($stub);

        return $stub;
    }

    /**
     * Compile the migration stub.
     *
     * @return string
     */
    protected function compileMigrationStub()
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/migration.stub');

        $this->replaceClassName($stub)
            ->replaceTableName($stub);

        return $stub;
    }

    /**
     * Compile the seeder stub.
     *
     * @return string
     */
    protected function compileSeedStub()
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/seeder.stub');

        $this->replaceSeederClass($stub)
            ->replaceModelNamespace($stub)
            ->replaceClassName($stub);

        return $stub;
    }

    /**
     * Replace the namespace in the model stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceModelNamespace(&$stub)
    {
        $stub = str_replace('{{namespace}}', $this->getAppNamespace() . 'Models', $stub);
        return $this;
    }

    /**
     * Replace the class name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceClassName(&$stub)
    {
        $stub = str_replace('{{class}}', $this->modelName, $stub);
        return $this;
    }

    /**
     * Replace the table name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceTableName(&$stub)
    {
        $stub = str_replace('{{tableName}}', str_plural(strtolower($this->modelName)), $stub);
        return $this;
    }

    /**
     * Replace the table name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceSeederClass(&$stub)
    {
        $stub = str_replace('{{seederClass}}', $this->transformUcPlural(), $stub);
        return $this;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            // [$name, $mode, $description, $defaultValue]
            ['name', InputArgument::REQUIRED, 'The name of the model'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            // [$name, $shortcut, $mode, $description, $defaultValue]
            ['with-repository', 'wr', InputOption::VALUE_OPTIONAL, 'Create a repository file for this model', null]
        ];
    }
}
