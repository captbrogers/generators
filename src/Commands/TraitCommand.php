<?php

namespace Captbrogers\Generators\Commands;

use Captbrogers\Generators\Traits\AppNamespaceDetectorTrait;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TraitCommand extends Command
{
    use AppNamespaceDetectorTrait;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gen:trait';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new (opinionated) trait';

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * @var Composer
     */
    private $composer;

    /**
     * @var $traitName
     */
    private $traitName;

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     * @param Composer $composer
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = app()['composer'];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->traitName = $this->getTraitName($this->argument('name'));

        if ($this->files->exists($traitFile = $this->getTraitPath($this->traitName))) {
            return $this->error($this->traitName . '.php trait already exists!');
        }

        $this->makeDirectory($traitFile);
        $this->files->put($traitFile, $this->compileTraitStub());
        $this->info('Trait created successfully.');

        $this->composer->dumpAutoloads();
    }

    /**
     * Get the class name for the Eloquent trait generator.
     *
     * @return string
     */
    protected function getTraitName($inputString)
    {
        $traitName = ucwords(str_singular(camel_case($inputString)));
        if (substr($traitName, -5, 5) !== 'Trait') {
            $traitName .= 'Trait';
        }
        return $traitName;
    }

    /**
     * Get the path to where we should store the model.
     *
     * @param  string $name
     * @return string
     */
    protected function getTraitPath()
    {
        return $this->laravel['path'] . '/Repositories/Eloquent/Traits/' . $this->traitName . '.php';
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0755, true, true);
        }
    }

    /**
     * Compile the model stub.
     *
     * @return string
     */
    protected function compileTraitStub($withValidation = false)
    {
        $stub = $this->files->get(__DIR__ . '/../stubs/trait.stub');

        $this->replaceTraitNamespace($stub)
            ->replaceClassName($stub);

        return $stub;
    }

    /**
     * Replace the namespace in the trait stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceTraitNamespace(&$stub)
    {
        $stub = str_replace('{{namespace}}', $this->getAppNamespace() . 'Repositories\Eloquent\Traits', $stub);
        return $this;
    }

    /**
     * Replace the class name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceClassName(&$stub)
    {
        $stub = str_replace('{{class}}', $this->traitName, $stub);
        return $this;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            // [$name, $mode, $description, $defaultValue]
            ['name', InputArgument::REQUIRED, 'The name of the trait'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            // [$name, $shortcut, $mode, $description, $defaultValue]
            //['with-repository', 'wr', InputOption::VALUE_OPTIONAL, '', null]
        ];
    }
}
