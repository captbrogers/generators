<?php

namespace Captbrogers\Generators;

class GeneratorException extends \Exception
{
    /**
     * The exception description.
     *
     * @var string
     */
    protected $message = "I can't read your mind, so you'll have to be more specific.";
}
