<?php

namespace Captbrogers\Generators;

use Illuminate\Support\ServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerModelGenerator();
        $this->registerRepositoryGenerator();
        $this->registerTraitGenerator();
    }

    /**
     * Register the gen:model generator.
     */
    private function registerModelGenerator()
    {
        $this->app->singleton('command.captbrogers.model', function ($app) {
            return $app['Captbrogers\Generators\Commands\ModelCommand'];
        });

        $this->commands('command.captbrogers.model');
    }

    /**
     * Register the gen:repository generator.
     */
    private function registerRepositoryGenerator()
    {
        $this->app->singleton('command.captbrogers.repository', function ($app) {
            return $app['Captbrogers\Generators\Commands\RepositoryCommand'];
        });

        $this->commands('command.captbrogers.repository');
    }

    /**
     * Register the gen:trait generator.
     */
    private function registerTraitGenerator()
    {
        $this->app->singleton('command.captbrogers.trait', function ($app) {
            return $app['Captbrogers\Generators\Commands\TraitCommand'];
        });

        $this->commands('command.captbrogers.trait');
    }
}
