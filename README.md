# generators

A highly opinionated file/class generator for Laravel 5.1+

This provides an alternative to the default model generator and adds an option to generate a repository pattern class for a model.

## Examples

 - [Models](#models)
 - [Repositories](#repositories)

## Installation

1. Install via Composer

```
composer require captbrogers/generators --dev
```

2. Load via Service Provider

It is recommended to load this package only in local/development environments. To do so, update your `app/Providers/AppServiceProvider.php` file (`register` method) with the following snippet:

```
if ($this->app->environment() === 'local') {
    $this->app->register('Captbrogers\Generators\GeneratorServiceProvider');
}
```

#### Models

This will create an Eloquent model but with more property overrides by default, optionally including a repository pattern file.

To generate a new model in the `app/Models` directory, use the following command replacing `{{ name }}` with the desired Model class name:

```
php artisan gen:model {{ name }}
```

You may also create a repository file (seen below) with the following command:

```
php artisan gen:model {{ name }} --with-repository=true
```

#### Repositories

Repositories attempt to adhere to the "[Repository Pattern](http://stackoverflow.com/a/11985962)", you can see more on it [here](https://github.com/rinvex/repository). It assumes they are relating to Eloquent usage, and as such will create an `app/Repositories/Eloquent` and `app/Repositories/Contracts` directories. It will name the generated files respectively as such:

`{{ name }}Repository.php`

`{{ name }}RepositoryContract.php`

It will also implement the contract (interface) so that if you decide to change from Eloquent at a later date it can assure that a minimum functionality will be required.

To generate a new Eloquent repository file with contract, use the following command replacing `{{ name }}` with a corresponding Model class name:

```
php artisan gen:repository {{ name }}
```

###### Traits for Repository

Included in this package is a trait that will have a good amount of methods that you can hook into to keep in line with the Repository Pattern. You can get it installed using the following the command:

```
php artisan gen:trait {{ name }}
```
